package com.agiletestingalliance;

public class MinMax {
  public int factory(int ant, int bowl) {
    if (ant < bowl){
      return bowl;
    }
    else{
      return ant;
    }
  }

  public String bar(String string) {
    if ("".equals(string) || string != null){
      return string;
    }

    if ("".equals(string) && string == null){
      return string;
    }
    return string;
  }
}
